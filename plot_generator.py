# ------------------------plot_generator.py  ----------------------------------
#
#                             Copyright
#                                2019
#                            MATTEO PERINI
#                      perini.matteo(at)gmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http:#www.gnu.org/licenses/>.
#
# See LICENSE.txt for information on usage and redistribution.
#
# -----------------------------------------------------------------------------

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import tkinter
from tkinter import ttk
from tkinter import messagebox
from tkinter import filedialog
from tkinter import scrolledtext

import os
import sys

class Modify_data(tkinter.Toplevel):
    def __init__(self,parent,textfile,num):
        self.parent = parent
        tkinter.Toplevel.__init__(self, self.parent)
        self.num = num
        self.textfile = textfile
        self.delimiters = [",",";"," ",":","_","|"]

        self.parent.withdraw()
        self.protocol("WM_DELETE_WINDOW", self.q)
        self.btn_try = tkinter.Button(self,text=u"Export data",bg="lightgreen",command=self.export, state=tkinter.DISABLED)
        self.btn_try.grid(column=2,columnspan=2,rowspan = 5,row=0,sticky='NSEW')
        self.txt_orig = scrolledtext.ScrolledText(self,width=50,height=30, wrap=tkinter.NONE)
        self.txt_orig.grid(column=0,columnspan = 2,row=5)
        self.txt_orig.insert(tkinter.INSERT,"".join(open(self.textfile).readlines()[:20]))
        self.txt_mod = scrolledtext.ScrolledText(self,width=50,height=30, wrap=tkinter.NONE)
        self.txt_mod.grid(column=2,columnspan = 2,row=5)
        
        self.lbl_ns = tkinter.Label(self, text="skipping lines", width=25, anchor='w')
        self.lbl_ns.grid(column=0,row=1,sticky='NSEW')
        var_ns = tkinter.DoubleVar(value=6)  # initial value
        self.ns = tkinter.Spinbox(self, from_=0, to=50, width=1, textvariable = var_ns, command = self.try_read)
        self.ns.grid(column=1,row=1,sticky='NSEW')
        
        self.lbl_x = tkinter.Label(self, text="X data column", width=25, anchor='w')
        self.lbl_x.grid(column=0,row=2,sticky='NSEW')
        var_x = tkinter.DoubleVar(value=1)  # initial value
        self.x_data = tkinter.Spinbox(self, from_=0, to=10, width=1, textvariable = var_x, command = self.try_read)
        self.x_data.grid(column=1,row=2,sticky='NSEW')
        
        self.lbl_y = tkinter.Label(self, text="Y data column", width=25, anchor='w')
        self.lbl_y.grid(column=0,row=3,sticky='NSEW')
        var_y = tkinter.DoubleVar(value=0)  # initial value
        self.y_data = tkinter.Spinbox(self, from_=0, to=10, width=1, textvariable = var_y, command = self.try_read)
        self.y_data.grid(column=1,row=3,sticky='NSEW')
        
        
        self.lbl_del = tkinter.Label(self, text="Delimiter", width=25, anchor='w')
        self.lbl_del.grid(column=0,row=4,sticky='NSEW')
        self.delim = ttk.Combobox(self,values=self.delimiters)
        self.delim.grid(column=1,row=4,sticky='NSEW')
        self.delim.current(0)
        self.delim.bind("<<ComboboxSelected>>", self.combo_sel)
        
        self.minsize(400,200)
        self.try_read()
    
    def export(self):
        self.parent.datalist[self.num-1]=self.data
        self.q()
    
    def handle_data(self,data):
        nn=20
        dlist =[]
        for i in range(nn):
            dlist.append("".join(str(data[i].tolist())))
        nd = "\n".join(dlist)
        return nd
        
    def combo_sel(self,a):
        self.try_read()
      
    def try_read(self):
        self.txt_mod.delete(1.0,tkinter.END)
        try:
            self.data = np.loadtxt(self.textfile, delimiter=self.delim.get(), dtype='float', skiprows=int(self.ns.get()), usecols=[int(self.x_data.get()),int(self.y_data.get())])
            newdata = self.handle_data(self.data)
            self.txt_mod.insert(tkinter.INSERT,newdata)
            self.btn_try.configure(state=tkinter.NORMAL)
        except ValueError:
            self.btn_try.configure(state=tkinter.DISABLED)
            self.txt_mod.insert(tkinter.INSERT,"Maybe you need to skip more rows!\nI can't convert plain text to data!")
        except IndexError:
            self.btn_try.configure(state=tkinter.DISABLED)
            if not hasattr(self,"data"):
                self.txt_mod.insert(tkinter.INSERT,"No data detected!\nTry to change separator!")
            elif int(self.x_data.get())>(self.data.shape[1]-1) or int(self.y_data.get())>(self.data.shape[1]-1):
                self.txt_mod.insert(tkinter.INSERT,"You are trying to use a non-existing column!")
            else:
                self.txt_mod.insert(tkinter.INSERT,"Unrecognized Index error!")
        except Exception as ex:
            if self.parent.debug:
                self.btn_try.configure(state=tkinter.DISABLED)
                template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                message = template.format(type(ex).__name__, ex.args)
                self.txt_mod.insert(tkinter.INSERT,message)
                print(message)
        
    def q(self):
        self.parent.update()
        self.parent.focus_set()
        self.parent.deiconify()
        self.destroy()


class Drawing(tkinter.Toplevel):
    def __init__(self, parent):
        self.parent = parent
        tkinter.Toplevel.__init__(self, self.parent)
        self.parent.withdraw()
        self.protocol("WM_DELETE_WINDOW", self.q)
        self.fc = FigureCanvasTkAgg(self.parent.fig, self)
        self.fc.get_tk_widget().pack()
        self.focus_force()
        self.grab_set()
        self.fc.draw()
        
    def q(self):
        self.parent.update()
        self.parent.focus_set()
        self.parent.deiconify()
        self.destroy()

class G_graph(tkinter.Tk):
    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.protocol("WM_DELETE_WINDOW", self.OnButtonExitClick)
        self.initialization()
        
    def initialization(self):
        self.debug = False
        self.datalist = [None,None,None,None,None]
        self.colorlist = ["red","blue","green","cyan","gold","maroon","tomato","salmon","turquoise","gray","pink","yellow","orange","purple"]
        self.stylelist = ['solid','dashed','dashdot','dotted']#,'None']

        #ROW 0
        self.label01 = tkinter.Label(self,text="", width = 10)
        self.label01.grid(column=0,row=0,sticky='NSEW')
        self.label01 = tkinter.Label(self,text="filename")
        self.label01.grid(column=1,row=0,sticky='NSEW')
        self.label02 = tkinter.Label(self,text="Style")
        self.label02.grid(column=2,row=0,sticky='NSEW')
        self.label03 = tkinter.Label(self,text="Color")
        self.label03.grid(column=3,row=0,sticky='NSEW')
        self.label04 = tkinter.Label(self,text="Legend Label")
        self.label04.grid(column=4,row=0,sticky='NSEW')
        
        #ROW 1
        n=1
        self.btn1 = tkinter.Button(self,text="Add file",command=self.add1)
        self.btn1.grid(column=0,row=n,sticky='NSEW')        
        self.fn1 = tkinter.StringVar()
        self.fn1.set("<-- click to add a file")
        self.filename1 = tkinter.Label(self, textvariable = self.fn1, width=50, anchor='w')
        self.filename1.grid(column=1,row=n,sticky='NSEW')
        self.style1 = ttk.Combobox(self,values=self.stylelist)
        self.style1.grid(column=2,row=n,sticky='NSEW')
        self.style1.current((n-1)%len(self.stylelist))
        self.color1 = ttk.Combobox(self,values=self.colorlist)
        self.color1.grid(column=3,row=n,sticky='NSEW')
        self.color1.current((n-1)%len(self.colorlist))
        self.label1 = tkinter.Entry(self,width=30)
        self.label1.grid(column=4,row=n,sticky='NSEW')
        
        #ROW 2
        n=2
        self.btn2 = tkinter.Button(self,text="Add file",command=self.add2)
        self.btn2.grid(column=0,row=n,sticky='NSEW')
        self.fn2 = tkinter.StringVar()
        self.fn2.set("<-- click to add a file")
        self.filename2 = tkinter.Label(self, textvariable = self.fn2, width=50, anchor='w')
        self.filename2.grid(column=1,row=n,sticky='NSEW')
        self.style2 = ttk.Combobox(self,values=self.stylelist)
        self.style2.grid(column=2,row=n,sticky='NSEW')
        self.style2.current((n-1)%len(self.stylelist))
        self.color2 = ttk.Combobox(self,values=self.colorlist)
        self.color2.grid(column=3,row=n,sticky='NSEW')
        self.color2.current((n-1)%len(self.colorlist))
        self.label2 = tkinter.Entry(self,width=30)
        self.label2.grid(column=4,row=n,sticky='NSEW')
        
        #ROW 3
        n=3
        self.btn3 = tkinter.Button(self,text="Add file",command=self.add3)
        self.btn3.grid(column=0,row=n,sticky='NSEW')
        self.fn3 = tkinter.StringVar()
        self.fn3.set("<-- click to add a file")
        self.filename3 = tkinter.Label(self, textvariable = self.fn3, width=50, anchor='w')
        self.filename3.grid(column=1,row=n,sticky='NSEW')
        self.style3 = ttk.Combobox(self,values=self.stylelist)
        self.style3.grid(column=2,row=n,sticky='NSEW')
        self.style3.current((n-1)%len(self.stylelist))
        self.color3 = ttk.Combobox(self,values=self.colorlist)
        self.color3.grid(column=3,row=n,sticky='NSEW')
        self.color3.current((n-1)%len(self.colorlist))
        self.label3 = tkinter.Entry(self,width=30)
        self.label3.grid(column=4,row=n,sticky='NSEW')
        
        #ROW 4
        n=4
        self.btn4 = tkinter.Button(self,text="Add file",command=self.add4)
        self.btn4.grid(column=0,row=n,sticky='NSEW')
        self.fn4 = tkinter.StringVar()
        self.fn4.set("<-- click to add a file")
        self.filename4 = tkinter.Label(self, textvariable = self.fn4, width=50, anchor='w')
        self.filename4.grid(column=1,row=n,sticky='NSEW')
        self.style4 = ttk.Combobox(self,values=self.stylelist)
        self.style4.grid(column=2,row=n,sticky='NSEW')
        self.style4.current((n-1)%len(self.stylelist))
        self.color4 = ttk.Combobox(self,values=self.colorlist)
        self.color4.grid(column=3,row=n,sticky='NSEW')
        self.color4.current((n-1)%len(self.colorlist))
        self.label4 = tkinter.Entry(self,width=30)
        self.label4.grid(column=4,row=n,sticky='NSEW')
        
        #ROW 5
        n=5
        self.btn5 = tkinter.Button(self,text="Add file",command=self.add5)
        self.btn5.grid(column=0,row=n,sticky='NSEW')
        self.fn5 = tkinter.StringVar()
        self.fn5.set("<-- click to add a file")
        self.filename5 = tkinter.Label(self, textvariable = self.fn5, width=50, anchor='w')
        self.filename5.grid(column=1,row=n,sticky='NSEW')
        self.style5 = ttk.Combobox(self,values=self.stylelist)
        self.style5.grid(column=2,row=n,sticky='NSEW')
        self.style5.current((n-1)%len(self.stylelist))
        self.color5 = ttk.Combobox(self,values=self.colorlist)
        self.color5.grid(column=3,row=n,sticky='NSEW')
        self.color5.current((n-1)%len(self.colorlist))
        self.label5 = tkinter.Entry(self,width=30)
        self.label5.grid(column=4,row=n,sticky='NSEW')
        
        #AXES
        self.s1 = tkinter.ttk.Separator(self, orient='horizontal')
        self.s1.grid(column=0,row=6,columnspan=7,sticky='WE')
        self.grid_rowconfigure(6, minsize=10)
        self.grid_rowconfigure(11, minsize=10)
        self.s2 = tkinter.ttk.Separator(self, orient='horizontal')
        self.s2.grid(column=0,row=11,columnspan=7,sticky='WE')
        self.plot_title_label = tkinter.Label(self, text = "Plot Title", width=20, anchor='e')
        self.plot_title_label.grid(column=0,row=7,sticky='NSEW')
        self.plot_title = tkinter.Entry(self)
        self.plot_title.grid(column=1,row=7,columnspan=2,sticky='NSEW')
        self.fontsize_title_label = tkinter.Label(self, text = "Fontsize", anchor='e')
        self.fontsize_title_label.grid(column=3,row=7,sticky='NSEW')
        var = tkinter.DoubleVar(value=18)  # initial value
        self.fontsize_title = tkinter.Spinbox(self, from_=6, to=24, width=1, textvariable = var)
        self.fontsize_title.grid(column=4,row=7,sticky='NSEW')
        self.x_label_label = tkinter.Label(self, text = "X Label", anchor='e')
        self.x_label_label.grid(column=0,row=8,sticky='NSEW')
        self.x_label = tkinter.Entry(self)
        self.x_label.grid(column=1,row=8,columnspan=2,sticky='NSEW')
        self.fontsize_x_label = tkinter.Label(self, text = "Fontsize", anchor='e')
        self.fontsize_x_label.grid(column=3,row=8,sticky='NSEW')
        var2 = tkinter.DoubleVar(value=14)  # initial value
        self.fontsize_x = tkinter.Spinbox(self, from_=6, to=24, width=1, textvariable = var2)
        self.fontsize_x.grid(column=4,row=8,sticky='NSEW')
        self.y_label_label = tkinter.Label(self, text = "Y Label", anchor='e')
        self.y_label_label.grid(column=0,row=9,sticky='NSEW')
        self.y_label = tkinter.Entry(self)
        self.y_label.grid(column=1,row=9,columnspan=2,sticky='NSEW')
        self.fontsize_y_label = tkinter.Label(self, text = "Fontsize", anchor='e')
        self.fontsize_y_label.grid(column=3,row=9,sticky='NSEW')
        self.fontsize_y = tkinter.Spinbox(self, from_=6, to=24, width=1, textvariable = var2)
        self.fontsize_y.grid(column=4,row=9,sticky='NSEW')
        self.lw_label = tkinter.Label(self, text = "Lines' width", anchor='e')
        self.lw_label.grid(column=3,row=10,sticky='NSEW')
        var3 = tkinter.DoubleVar(value=2)
        self.lw = tkinter.Spinbox(self, from_=1, to=6, width=1, textvariable = var3)
        self.lw.grid(column=4,row=10,sticky='NSEW')
        
        self.btnTake = tkinter.Button(self,text=u"Draw Plot",bg="lightgreen",command=self.OnButtonTakeClick)
        self.btnTake.grid(column=0,row=12,columnspan =3,sticky='NSEW')
        
        self.btnSave = tkinter.Button(self,text=u"Save Plot",bg="orange",command=self.save_pdf, state=tkinter.DISABLED)
        self.btnSave.grid(column=3,row=12,sticky='NSEW')
        
        self.btnExit = tkinter.Button(self,text=u"Exit",bg="orange red",command=self.OnButtonExitClick)
        self.btnExit.grid(column=4,row=12,sticky='NSEW')

        self.minsize(720,320)
        self.update()
        
    def returnto(self):
        self.initialization()
        
    def save_pdf(self):
        a = filedialog.asksaveasfilename(filetypes=(("PDF file", "*.pdf"),("All Files", "*.*")), 
            defaultextension='.pdf', title="Plot's name")
        if a:
            self.fig.savefig(a, bbox_inches='tight',format='pdf', dpi=1000)
        else:
            pass

    def loadfile(self,n):
        self.textfile = filedialog.askopenfilename(filetypes = (("Text files","*.txt"),("CSV files","*.csv"),("all files","*.*")))
        self.btnSave.configure(state=tkinter.DISABLED)
        if self.textfile == () or self.textfile == "":
            self.datalist[n-1] = None
            if n == 1:
                self.fn1.set("<-- click to add a file")
            elif n == 2:
                self.fn2.set("<-- click to add a file")
            elif n == 3:
                self.fn3.set("<-- click to add a file")
            elif n == 4:
                self.fn4.set("<-- click to add a file")
            elif n == 5:
                self.fn5.set("<-- click to add a file")
        else:
            filename, file_extension = os.path.splitext(os.path.basename(self.textfile))
            if n == 1:
                self.fn1.set(filename)
            elif n == 2:
                self.fn2.set(filename)
            elif n == 3:
                self.fn3.set(filename)
            elif n == 4:
                self.fn4.set(filename)
            elif n == 5:
                self.fn5.set(filename)
            try:
                f = open(self.textfile)
            except:
                messagebox.showerror("Error", "Something goes wrong!")
            Modify_data(self,self.textfile, n)
        
    def add1(self):
        self.loadfile(1)

    def add2(self):
        self.loadfile(2)

    def add3(self):
        self.loadfile(3)

    def add4(self):
        self.loadfile(4)

    def add5(self):
        self.loadfile(5)

    def OnButtonTakeClick(self):
        self.fig, self.ax = plt.subplots()
        self.fig.suptitle(self.plot_title.get(), fontsize=int(self.fontsize_title.get()), fontweight='bold')
        plt.xlabel(self.x_label.get(),fontsize=int(self.fontsize_x.get()),fontweight='bold')
        plt.ylabel(self.y_label.get(),fontsize=int(self.fontsize_y.get()),fontweight='bold')

        for n,d in enumerate(self.datalist):
            try: 
                d.shape
                if n == 0:
                    col = self.color1.get()
                    lab = self.label1.get()
                    st = self.style1.get()
                elif n == 1:
                    col = self.color2.get()
                    lab = self.label2.get()
                    st = self.style2.get()
                elif n == 2:
                    col = self.color3.get()
                    lab = self.label3.get()
                    st = self.style3.get()
                elif n == 3:
                    col = self.color4.get()
                    lab = self.label4.get()
                    st = self.style4.get()
                elif n == 4:
                    col = self.color5.get()
                    lab = self.label5.get()
                    st = self.style5.get()
                x=d[:,0]
                y=d[:,1]
                self.ax.plot(x, y, color=col,linestyle=st,label=lab, linewidth=int(self.lw.get()))
                
            except Exception as ex:
                if self.debug:
                    template = "An exception of type {0} occurred. Arguments:\n{1!r}"
                    message = template.format(type(ex).__name__, ex.args)
                    print(message)
        self.leg = self.ax.legend()
        self.btnSave.configure(state=tkinter.NORMAL)
        Drawing(self)
        
    def OnButtonExitClick(self):
        self.destroy()
        app.quit()
        


if __name__ == "__main__":
    app = G_graph(None)
    app.title('Plot generator')
    app.mainloop()


