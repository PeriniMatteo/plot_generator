# Plot_generator

### This is a simple plot generator using matplotlib (python3)

### The main aim is to enhace the stress-strain plot production providing a graphical interface.

### The software is designed to have as input files becoming from traction machines.

### You can select up to 5 files modify the data and customize your plot in a friendly manner.

### You can use both TXT or CSV file types.


Copyright: MATTEO PERINI (2019)
Perini.matteo(at)gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http:#www.gnu.org/licenses/>.

See LICENSE.txt for information on usage and redistribution.